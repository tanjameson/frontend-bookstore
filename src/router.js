import Vue from "vue";
import VueRouter from "vue-router";

import WelcomePage from "./components/WelcomePage.vue";
import TestingPage from "./components/TestingPage.vue";
import FindAll from "./components/FindAll.vue";
import SearchBook from "./components/SearchBook.vue";
import Create from "./components/Create.vue";
import Error from "./components/Error.vue";
import NotFound from "./components/NotFound.vue";
import Delete from "./components/Delete.vue";
import UploadMedia from "./components/UploadMedia.vue";
import Reordering from "./components/Reordering.vue";

Vue.use(VueRouter);

const routes = [
  { path: "/", component: WelcomePage },
  { path: "/findAll", component: FindAll },
  { path: "/search", component: SearchBook },
  { path: "/create", component: Create },
  { path: "/delete", component: Delete },
  { path: "/testing", component: TestingPage },
  { path: "/error", name: "Error", component: Error },
  { path: "/uploadMedia", component: UploadMedia },
  { path: "/reordering", component: Reordering },
  { path: "/:notFound(.*)", component: NotFound },
];

export default new VueRouter({ mode: "history", routes });
