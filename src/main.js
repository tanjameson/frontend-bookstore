import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import "../node_modules/vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css";

Vue.config.productionTip = false
Vue.component('vue-ctk-date-time-picker', VueCtkDateTimePicker);

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
